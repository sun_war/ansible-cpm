Download awslogs package
 ```angularjs
curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O
```
Create `awslogs.conf` config
```angularjs
vi awslogs.conf
```
Write config, example this below
```angularjs
[/var/log/messages]
datetime_format = %b %d %H:%M:%S
file = /var/log/messages
buffer_duration = 5000
log_stream_name = {instance_id}
initial_position = start_of_file
log_group_name = RHEL-7
```
Install with config
```angularjs
sudo python /tmp/awslogs-agent-setup.py -n -r {{ ansible_ec2_placement_region }} -c {file_config}
```
Example
```angularjs
sudo python ./awslogs-agent-setup.py -n -r ap-southeast-1 -c awslogs.conf 
```
 Execute the command below to start and enable awslogs the service.
 ```angularjs
systemctl start awslogs
chkconfig awslogs on
```
